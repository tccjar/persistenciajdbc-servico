package br.ueg.unucet.plugin.service.persistenciabdjdbc1.util.annotations;

import java.lang.reflect.Field;

/**
 * @author Tiago Classe auxiliar para leitura dos campos da Annotation,
 *         PropriedadesCampo
 */
public class LeitorAnnotacoes {

	/**
	 * @param f
	 *            , a inst�ncia de algum campo adquirido via computa��o
	 *            reflexiva
	 * @return O nome da coluna informado na annotation PropriedadesCampo
	 */
	public static String getNomeColuna(Field f) {
		String campo = null;
		PropriedadesCampo anotacao = getPropriedadesCampo(f);
		if (anotacao != null && !anotacao.campoNaoPersistido()) {
			campo = anotacao.nomeColuna();
		}
		return campo;
	}

	/**
	 * @param f
	 *            , a inst�ncia de algum campo adquirido via computa��o
	 *            reflexiva
	 * @return true ou false, informando se o campo � obrigat�rio ou n�o
	 */
	public static boolean isCampoObrigatorio(Field f) {
		boolean retorno = false;
		PropriedadesCampo anotacao = getPropriedadesCampo(f);
		if (anotacao != null && !anotacao.campoNaoPersistido()) {
		  retorno = anotacao.campoObrigatorio();
		}
		return retorno;
	}

	/**
	 * @param f
	 *            , a inst�ncia de algum campo adquirido via computa��o
	 *            reflexiva
	 * @return true ou false, informando se o campo � �nico (UNIQUE) ou n�o
	 */
	public static boolean isCampoUnico(Field f) {
		boolean retorno = false;
		PropriedadesCampo anotacao = getPropriedadesCampo(f);
		if (anotacao != null && !anotacao.campoNaoPersistido()) {
		  retorno = anotacao.campoUnico();
		}
		return retorno;
	}
	
	/**
	 * @param f
	 *            , a inst�ncia de algum campo adquirido via computa��o
	 *            reflexiva
	 * @return true ou false, informando se o campo � auto Incremento ou n�o
	 */
	public static boolean isCampoAutoIncremento(Field f) {
		boolean retorno = false;
		PropriedadesCampo anotacao = getPropriedadesCampo(f);
		if (anotacao != null && !anotacao.campoNaoPersistido()) {
		  retorno = anotacao.campoAutoIncremento();
		}
		return retorno;
	}
	
	
	/**
	 * @param f
	 *            , a inst�ncia de algum campo adquirido via computa��o
	 *            reflexiva
	 * @return a Anotacao
	 * @throws RuntimeExcepticon
	 *             , caso n�o haja a anota��o PropriedadeCampo no campo
	 *             rastreado.
	 */

	///Concertar melhor a questao da Exception
	public static PropriedadesCampo getPropriedadesCampo(Field f) {
		PropriedadesCampo anotacao = null;
		f.setAccessible(true);
		if (f.isAnnotationPresent(PropriedadesCampo.class)) {
			anotacao = f.getAnnotation(PropriedadesCampo.class);
		} else {
				throw new RuntimeException("O campo: '" + f.getName()
						+ "' n�o possue nenhuma anota��o PropriedadesCampo");
			}

		return anotacao;
	}
}
