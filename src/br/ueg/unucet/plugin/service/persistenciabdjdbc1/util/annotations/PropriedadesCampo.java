package br.ueg.unucet.plugin.service.persistenciabdjdbc1.util.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

//Guiliano questionou quest�o da necessidade de anotations para dizer se o campo � auto-incremento,
//se a entidade � transienty (campo que n�o pode ser persistido)

/**
 * @author Tiago
 * Annotation que define se as propriedades do campo:
 * se ele e`ou nao obrigatorio, se e`ou nao unico
 * e tambem define o nome da coluna do banco de dados.
 * Todas essa propriedades, mais tarde poderao ativar validacoes
 * e serem usadas para a persistencia no banco de dados
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface PropriedadesCampo {
   String nomeColuna() default "";
   boolean campoObrigatorio() default false;
   boolean campoUnico() default false;
   boolean campoAutoIncremento() default false;
   boolean campoNaoPersistido() default false;
}
