package br.ueg.unucet.plugin.service.persistenciabdjdbc1.util.annotations;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import br.ueg.unucet.plugin.service.persistenciabdjdbc1.model.Entidade;

public class Reflexao {

	public static Field[] getAllFields(Class<?> clazz)
	  {
	      List<Class<?>> classes = getAllSuperclasses(clazz);
	      classes.add(clazz);
	      return getAllFields(classes);
	  }
	  
	private static Field[] getAllFields(List<Class<?>> classes)
	  {
	      Set<Field> fields = new HashSet<Field>();
	      for (Class<?> clazz : classes)
	      {
	          fields.addAll(Arrays.asList(clazz.getDeclaredFields()));
	      }

	      return fields.toArray(new Field[fields.size()]);
	  }

	  
	  public static List<Class<?>> getAllSuperclasses(Class<?> clazz)
	  {
	      List<Class<?>> classes = new ArrayList<Class<?>>();

	      Class<?> superclass = clazz.getSuperclass();
	      while (superclass != null)
	      {
	          classes.add(superclass);
	          superclass = superclass.getSuperclass();
	      }

	      return classes;
	  }
	  
	  public static Object InvokeGetMethod(Object objeto, Field f)
	  {
		  String isOrGet = "get";
		  Object valor = null;
		  try{
		  if (f.getType().getSimpleName().equals("Boolean"))
			  isOrGet = "is";
		  Method metodo;
		
			metodo = objeto.getClass().getMethod(isOrGet
						+ f.getName().substring(0, 1).toUpperCase() +
						f.getName().substring(1)
						, null);
			valor =  metodo.invoke(objeto, null);
			
	  } catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return valor;
	  }
	  
	  public static void InvokeSetMethod(Field f, Object newValue)
	  {
		  
	  }
	  
	public static void InvokeStmtMethod(Field f, Object value,
			PreparedStatement stm, Integer position) throws SQLException {
		System.out.println(f.getType().getSimpleName());
		if (f.getType().getSimpleName().equals("String")) {
			stm.setString(position, (String) value);
		} else {
			if (f.getType().getSimpleName().equals("Integer")) {
				stm.setInt(position, (Integer) value);
			} else if (f.getType().getSimpleName().equals("Long")) {
				stm.setLong(position, (Long) value);
			} else
			if (f.getType().isEnum()) {
				try {
					Method metodo = f.getType().getMethod("getId",null);
					Integer intValue = (Integer) metodo.invoke(value, new Class<?>[0]);
					stm.setInt(position, intValue);
				} catch (SecurityException e) {
					e.printStackTrace();
				} catch (NoSuchMethodException e) {
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					
					e.printStackTrace();
				}
			} else

			if (value instanceof Entidade) {
				stm.setLong(position, ((Entidade) value).getCodigo());
			} else if (f.getType().getSimpleName().equals("Boolean")) {
				stm.setBoolean(position, (Boolean) value);
			} else if(f.getType().getSimpleName().equals("Date"))
			{
				stm.setTimestamp(position, new Timestamp( ((java.util.Date) value).getTime() ) );
			} else if(f.getType().getSimpleName().equalsIgnoreCase("byte[]")){
				stm.setBytes(position, (byte[]) value);
			}
			else
			{
				throw new RuntimeException(
						"Erro de programa��o, algum atributo da classe n�o � suportado pela arquitetura!");
			}
			// /tipos suportados
			// Boolean
			// String
			// Integer
			// Long
			// Enum
			// Entidade
			// Date
		}
	}
	
	public static Object InstanceClass(String nameClass)
	{
		try {
			System.out.println(nameClass);
			Class<?> classe = Class.forName(nameClass);
			return classe.newInstance();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
	}

}
