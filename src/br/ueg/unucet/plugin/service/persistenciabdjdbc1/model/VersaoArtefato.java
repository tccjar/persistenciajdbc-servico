package br.ueg.unucet.plugin.service.persistenciabdjdbc1.model;

import br.ueg.unucet.plugin.service.persistenciabdjdbc1.util.annotations.PropriedadesCampo;


/**
 * POJO que representa as versões de um artefato
 * 
 * @author Tiago
 *
 */
public class VersaoArtefato extends Entidade{
	
	@PropriedadesCampo(nomeColuna="usuario_preenchedor_codigo")
	private Long usuarioPreenchedorCodigo;
	
	@PropriedadesCampo(nomeColuna="ultima_versao")
	private Long ultimaVersao;
	
	@PropriedadesCampo(nomeColuna="ultima_revisao")
	private Long ultimaRevisao;  
	
	@PropriedadesCampo(nomeColuna="projeto_codigo")
	private Long projetoCodigo;  
	
	@PropriedadesCampo(nomeColuna="artefato_codigo")
	private Long artefatoCodigo;
	
	
	
	@Override
	public String getNomeTabela() {
		
		return "artefato_versao";
	}



	public Long getUsuarioPreenchedorCodigo() {
		return usuarioPreenchedorCodigo;
	}



	public void setUsuarioPreenchedorCodigo(Long usuarioPreenchedorCodigo) {
		this.usuarioPreenchedorCodigo = usuarioPreenchedorCodigo;
	}



	public Long getUltimaVersao() {
		return ultimaVersao;
	}



	public void setUltimaVersao(Long ultimaVersao) {
		this.ultimaVersao = ultimaVersao;
	}



	public Long getUltimaRevisao() {
		return ultimaRevisao;
	}



	public void setUltimaRevisao(Long ultimaRevisao) {
		this.ultimaRevisao = ultimaRevisao;
	}



	public Long getProjetoCodigo() {
		return projetoCodigo;
	}



	public void setProjetoCodigo(Long projetoCodigo) {
		this.projetoCodigo = projetoCodigo;
	}



	public Long getArtefatoCodigo() {
		return artefatoCodigo;
	}



	public void setArtefatoCodigo(Long artefatoCodigo) {
		this.artefatoCodigo = artefatoCodigo;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((artefatoCodigo == null) ? 0 : artefatoCodigo.hashCode());
		result = prime * result
				+ ((projetoCodigo == null) ? 0 : projetoCodigo.hashCode());
		result = prime * result
				+ ((ultimaRevisao == null) ? 0 : ultimaRevisao.hashCode());
		result = prime * result
				+ ((ultimaVersao == null) ? 0 : ultimaVersao.hashCode());
		result = prime
				* result
				+ ((usuarioPreenchedorCodigo == null) ? 0
						: usuarioPreenchedorCodigo.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		VersaoArtefato other = (VersaoArtefato) obj;
		if (artefatoCodigo == null) {
			if (other.artefatoCodigo != null)
				return false;
		} else if (!artefatoCodigo.equals(other.artefatoCodigo))
			return false;
		if (projetoCodigo == null) {
			if (other.projetoCodigo != null)
				return false;
		} else if (!projetoCodigo.equals(other.projetoCodigo))
			return false;
		if (ultimaRevisao == null) {
			if (other.ultimaRevisao != null)
				return false;
		} else if (!ultimaRevisao.equals(other.ultimaRevisao))
			return false;
		if (ultimaVersao == null) {
			if (other.ultimaVersao != null)
				return false;
		} else if (!ultimaVersao.equals(other.ultimaVersao))
			return false;
		if (usuarioPreenchedorCodigo == null) {
			if (other.usuarioPreenchedorCodigo != null)
				return false;
		} else if (!usuarioPreenchedorCodigo
				.equals(other.usuarioPreenchedorCodigo))
			return false;
		return true;
	}

	
	

}
