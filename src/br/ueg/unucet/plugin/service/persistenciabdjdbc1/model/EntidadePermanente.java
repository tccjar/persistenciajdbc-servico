package br.ueg.unucet.plugin.service.persistenciabdjdbc1.model;

import br.ueg.unucet.plugin.service.persistenciabdjdbc1.util.annotations.PropriedadesCampo;



public abstract class EntidadePermanente extends Entidade {
 
	@PropriedadesCampo(nomeColuna="estado", campoObrigatorio=true)
	private Integer status;
	
	@PropriedadesCampo(campoNaoPersistido=true)
	private static final String campoInativacao = "estado";
	
	public static String getCampoInativacao() {
		return campoInativacao;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		EntidadePermanente other = (EntidadePermanente) obj;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		return true;
	}
	 
}
 
