package br.ueg.unucet.plugin.service.persistenciabdjdbc1.model;

import br.ueg.unucet.plugin.service.persistenciabdjdbc1.util.annotations.PropriedadesCampo;

/**
 * POJO que representa o ArtefatoModelo com seus valores a ser persistido como
 * Artefato no Serviço de Persistência
 * 
 * @author Tiago
 *
 */
public class Artefato extends Entidade{
		
	@PropriedadesCampo(nomeColuna="projeto_codigo")
	private Long projetoCodigo;
	
	@PropriedadesCampo(nomeColuna="artefato_codigo")
	private Long artefatoCodigo;
	
	@PropriedadesCampo(nomeColuna="versao")
	private Long versao;
	
	@PropriedadesCampo(nomeColuna="revisao")
	private Long revisao;
	
	@PropriedadesCampo(nomeColuna="lista_membro")
	private byte[] listaMembros;
	
	@PropriedadesCampo(nomeColuna="valores_membro")
	private byte[] listaValoresMembros;

	@Override
	public String getNomeTabela() {
		return "artefato";
	}

	public Long getProjetoCodigo() {
		return projetoCodigo;
	}

	public void setProjetoCodigo(Long projetoCodigo) {
		this.projetoCodigo = projetoCodigo;
	}

	public Long getArtefatoCodigo() {
		return artefatoCodigo;
	}

	public void setArtefatoCodigo(Long artefatoCodigo) {
		this.artefatoCodigo = artefatoCodigo;
	}

	public Long getVersao() {
		return versao;
	}

	public void setVersao(Long versao) {
		this.versao = versao;
	}

	public Long getRevisao() {
		return revisao;
	}

	public void setRevisao(Long revisao) {
		this.revisao = revisao;
	}

	public byte[] getListaMembros() {
		return listaMembros;
	}

	public void setListaMembros(byte[] listaMembros) {
		this.listaMembros = listaMembros;
	}

	public byte[] getListaValoresMembros() {
		return listaValoresMembros;
	}

	public void setListaValoresMembros(byte[] listaValoresMembros) {
		this.listaValoresMembros = listaValoresMembros;
	}
	
	
}
