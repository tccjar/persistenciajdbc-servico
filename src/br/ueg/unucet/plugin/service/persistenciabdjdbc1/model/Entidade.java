package br.ueg.unucet.plugin.service.persistenciabdjdbc1.model;

import br.ueg.unucet.plugin.service.persistenciabdjdbc1.util.annotations.PropriedadesCampo;


public abstract class Entidade {
 
	@PropriedadesCampo(nomeColuna="codigo", campoAutoIncremento=true)
	private Long codigo;
	@PropriedadesCampo(campoNaoPersistido=true)
	private static final String chavePrimaria = "codigo";
	
	public static String getChavePrimaria() {
	   return chavePrimaria;
	}
	
	public abstract String getNomeTabela();

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Entidade other = (Entidade) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}



	 
}
 
