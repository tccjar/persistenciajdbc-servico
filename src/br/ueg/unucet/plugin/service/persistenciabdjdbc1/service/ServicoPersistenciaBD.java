package br.ueg.unucet.plugin.service.persistenciabdjdbc1.service;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

import br.ueg.unucet.plugin.service.persistenciabdjdbc1.model.Artefato;
import br.ueg.unucet.plugin.service.persistenciabdjdbc1.model.VersaoArtefato;
import br.ueg.unucet.plugin.service.persistenciabdjdbc1.persistence.ArtefatoServico;
import br.ueg.unucet.plugin.service.persistenciabdjdbc1.persistence.FabricaConexao;
import br.ueg.unucet.plugin.service.persistenciabdjdbc1.persistence.VersaoArtefatoServico;
import br.ueg.unucet.quid.extensao.dominios.Membro;
import br.ueg.unucet.quid.extensao.enums.DominioEntradaEnum;
import br.ueg.unucet.quid.extensao.enums.MomentosDisparoServicoEnum;
import br.ueg.unucet.quid.extensao.enums.TiposServicoEnum;
import br.ueg.unucet.quid.extensao.implementacoes.Parametro;
import br.ueg.unucet.quid.extensao.implementacoes.ServicoPersistencia;
import br.ueg.unucet.quid.extensao.interfaces.IParametro;
import br.ueg.unucet.quid.extensao.utilitarias.FabricaParametros;
import br.ueg.unucet.quid.servicos.MembroServico;
import br.ueg.unucet.quid.utilitarias.FabricaSerializacao;

public class ServicoPersistenciaBD extends ServicoPersistencia {

	public static final String NOME_SERVICO = "persistenciabdjdbc";
	public static final String PARAMETRO_HOST = "HOST";
	public static final String PARAMETRO_BANCO_DADOS = "BANCO_DADOS";
	public static final String PARAMETRO_USUARIO_BANCO = "USUARIO_BANCO";
	public static final String PARAMETRO_PASSWORD = "PASSWORD";
	public static final String PARAMETRO_VERSAO_ATUAL = "PARAMETRO_VERSAO_ATUAL";
	public static final String PARAMETRO_VERSAO_ARTEFATO = "VERSAO_ARTEFATO";
	public static final String PARAMETRO_REVISAO_ARTEFATO = "REVISAO_ARTEFATO";
	public static final String CLASSE_JDBC = "org.postgresql.Driver";

	private ArtefatoServico artefatoServico;
	private VersaoArtefatoServico versaoArtefatoServico;
	private Collection<IParametro<?>> listaParametros;
	
	@Override
	public Collection<IParametro<?>> getListaParametros() {
		return this.listaParametros;
	}
	
	@Override
	public void setListaParametros(Collection<IParametro<?>> lista) {
		this.listaParametros = lista;
	}

	public ServicoPersistenciaBD() {
		super();
	}

	@Override
	protected void configuraParametros() {
		super.configuraParametros();
		getListaParametros().add(
				FabricaParametros.gerarParametro(String.class, PARAMETRO_HOST,
						"Host", DominioEntradaEnum.ALFANUMERICO, true));
		getListaParametros().add(
				FabricaParametros.gerarParametro(String.class,
						PARAMETRO_BANCO_DADOS, "Banco de dados",
						DominioEntradaEnum.ALFANUMERICO, true));
		getListaParametros().add(
				FabricaParametros.gerarParametro(String.class,
						PARAMETRO_USUARIO_BANCO, "Usuário",
						DominioEntradaEnum.ALFANUMERICO, true));
		getListaParametros().add(
				FabricaParametros.gerarParametro(String.class,
						PARAMETRO_PASSWORD, "Password",
						DominioEntradaEnum.ALFANUMERICO, true));
	}

	protected Collection<IParametro> efetuaOperacao() throws SQLException,
			Exception {
		FabricaConexao.configureConexao(String.valueOf(getParametroPorNome(
				PARAMETRO_HOST).getValor()),
				String.valueOf(getParametroPorNome(PARAMETRO_BANCO_DADOS)
						.getValor()), String.valueOf(getParametroPorNome(
						PARAMETRO_USUARIO_BANCO).getValor()), String
						.valueOf(getParametroPorNome(PARAMETRO_PASSWORD)
								.getValor()), CLASSE_JDBC);
		artefatoServico = new ArtefatoServico();
		versaoArtefatoServico = new VersaoArtefatoServico();

		Boolean sucesso = false;
		Collection<IParametro> parametros = new ArrayList<IParametro>();
		if (getModo() == null
				|| getModo().equalsIgnoreCase(ServicoPersistencia.MODO_SALVAR)) {
			try{
				sucesso = salvarArtefato();
			}catch(Exception e)
			{
				sucesso = false;
			}
		} else {
			Artefato artefato = lerArtefato();
			parametros.add(FabricaParametros.gerarParametroValor(byte[].class, PARAMETRO_LISTA_MEMBROS, artefato.getListaMembros()));
			parametros.add(FabricaParametros.gerarParametroValor(byte[].class, PARAMETRO_VALORES_MEMBROS, artefato.getListaValoresMembros()));
			sucesso = true;
		}
		
		Parametro<Boolean> retorno = new Parametro<Boolean>(Boolean.class);
		retorno.setNome("Sucesso");
		retorno.setValorClass(sucesso);
		parametros.add(retorno);
		return parametros;
	}

	private void criaBanco() throws SQLException {
		String ddl = "CREATE TABLE IF NOT EXISTS artefato" + "("
				+ "codigo serial NOT NULL,"
				+ "artefato_codigo bigint NOT NULL,"
				+ "projeto_codigo bigint NOT NULL,"
				+ "versao bigint NOT NULL,"
				+ "revisao bigint NOT NULL,"
				+ "lista_membro bytea,"
				+ "valores_membro bytea NOT NULL,"
				+ "CONSTRAINT pk_codigo PRIMARY KEY (codigo)" + ");"
				+ "CREATE TABLE IF NOT EXISTS artefato_versao" + "("
				+ "codigo serial NOT NULL,"
				+ "artefato_codigo bigint NOT NULL,"
				+ "projeto_codigo bigint NOT NULL,"
				+ "ultima_versao bigint NOT NULL,"
				+ "ultima_revisao bigint NOT NULL,"
				+ "usuario_preenchedor_codigo bigint,"
				+ "CONSTRAINT versao_pk PRIMARY KEY (codigo)" + ");"
				;
		PreparedStatement stm = FabricaConexao.getConnection()
				.prepareStatement(ddl);
		artefatoServico.executaQueryOperacao(stm);

	}

	private Boolean salvarArtefato() throws Exception {
		try {
			criaBanco();
		} catch (Exception e) {

		}
		VersaoArtefato ultimaVersao = procuraUltimaVersao();
		if (ultimaVersao == null) {
			ultimaVersao = new VersaoArtefato();
			ultimaVersao.setArtefatoCodigo(getParametroIdArtefatoPreenchido());
			ultimaVersao.setProjetoCodigo(getProjetoEscolhido());
			ultimaVersao.setUltimaVersao(getParametroVersao().longValue());
			ultimaVersao.setUltimaRevisao(1l);
		}
		if (ultimaVersao != null) {
			ultimaVersao
			.setUltimaVersao(getParametroVersao().longValue());
			if (getParametroNovaVersao()) {
				ultimaVersao
						.setUltimaRevisao(1l);
			} else {
				ultimaVersao
						.setUltimaRevisao(ultimaVersao.getUltimaRevisao() + 1l);
			}
			versaoArtefatoServico.persiste(ultimaVersao);
		}
		Artefato novoArtefato = new Artefato();
		novoArtefato.setArtefatoCodigo(getParametroIdArtefatoPreenchido());
		novoArtefato.setListaMembros(getParametroListaMembros());
		novoArtefato.setListaValoresMembros(getParametroValoresMembros());
		novoArtefato.setProjetoCodigo(getProjetoEscolhido());
		novoArtefato.setRevisao(ultimaVersao.getUltimaRevisao());
		novoArtefato.setVersao(ultimaVersao.getUltimaVersao());
		return artefatoServico.persiste(novoArtefato);
	}

	private VersaoArtefato procuraUltimaVersao() {
		versaoArtefatoServico.camposProcura.limpar();
		versaoArtefatoServico.camposProcura.setCondicional("AND");
		versaoArtefatoServico.camposProcura.add("artefatoCodigo",
				getParametroIdArtefatoPreenchido().toString(),true);
		versaoArtefatoServico.camposProcura.add("projetoCodigo",
				getProjetoEscolhido().toString(),true);
		ArrayList<VersaoArtefato> versoes = versaoArtefatoServico.procura();
		if (versoes.size() == 0)
			return null;
		return versoes.get(0);

	}

	private Artefato procuraArtefato(Long versao, Long revisao) {
		artefatoServico.camposProcura.limpar();
		artefatoServico.camposProcura.setCondicional("AND");
		artefatoServico.camposProcura.add("artefatoCodigo", getParametroIdArtefatoPreenchido().toString(),true);
		artefatoServico.camposProcura.add("projetoCodigo", getProjetoEscolhido().toString(),true);
		artefatoServico.camposProcura.add("versao", versao.toString(),true);
		artefatoServico.camposProcura.add("revisao", revisao.toString(),true);
		ArrayList<Artefato> artefatos = artefatoServico.procura();
		if (artefatos.size() == 0)
			return null;
		return artefatos.get(0);
	}

	private Artefato lerArtefato() {
		VersaoArtefato ultimaVersao = procuraUltimaVersao();
		if (ultimaVersao == null) {
			ultimaVersao = new VersaoArtefato();
			ultimaVersao.setArtefatoCodigo(getParametroIdArtefatoPreenchido());
			ultimaVersao.setProjetoCodigo(getProjetoEscolhido());
			ultimaVersao.setUltimaVersao(0l);
			ultimaVersao.setUltimaRevisao(0l);
		}
		return procuraArtefato(ultimaVersao.getUltimaVersao(),ultimaVersao.getUltimaRevisao());
	}

	public String getModo() {
		Object valor = getParametroPorNome(PARAMETRO_MODO).getValor();
		if (valor != null) {
			return String.valueOf(valor);
		}
		return null;
	}

	public Integer getRevisaoArtefato() {
		Object valor = getParametroPorNome(PARAMETRO_REVISAO_ARTEFATO)
				.getValor();
		if (valor != null) {
			return Integer.valueOf(String.valueOf(valor));
		}
		return null;
	}

	public Boolean getVersaoAtual() {

		Object valor = getParametroPorNome(PARAMETRO_VERSAO_ATUAL).getValor();
		if (valor != null) {
			return Boolean.valueOf(String.valueOf(valor));
		}
		return null;
	}

	public Integer getVersaoArtefato() {

		Object valor = getParametroPorNome(PARAMETRO_VERSAO_ARTEFATO)
				.getValor();
		if (valor != null) {
			return Integer.valueOf(String.valueOf(valor));
		}
		return null;
	}

	public Integer getParametroVersao() {
		Object valor = getParametroPorNome(PARAMETRO_VERSAO).getValor();
		if (valor != null) {
			return Integer.valueOf(String.valueOf(valor));
		}
		return null;
	}

	public Integer getParametroRevisao() {
		Object valor = getParametroPorNome(PARAMETRO_REVISAO).getValor();
		if (valor != null) {
			return Integer.valueOf(String.valueOf(valor));
		}
		return null;
	}

	public Long getParametroIdArtefatoPreenchido() {
		Object valor = getParametroPorNome(PARAMETRO_ID_ARTEFATO_PREENCHIDO)
				.getValor();
		if (valor != null) {
			return Long.valueOf(String.valueOf(valor));
		}
		return null;
	}

	@Override
	public String getNome() {
		return NOME_SERVICO;
	}

	@Override
	public String getDescricao() {
		return "Serviço que efetua Persistência de Artefato Preenchido em Banco de Dados";
	}

	@Override
	protected void inicializacaoConstrutor() {
	}

	@Override
	public Integer getVersao() {
		return 1;
	}

	public Integer getRevisao() {
		return 1;
	}

	@Override
	public TiposServicoEnum getTipoServico() {
		return TiposServicoEnum.PERSISTENCIA;
	}

	@Override
	public Collection<MomentosDisparoServicoEnum> getMomentosDisparoSuportados() {
		Collection<MomentosDisparoServicoEnum> momentosDisparo = new ArrayList<MomentosDisparoServicoEnum>();
		momentosDisparo.add(MomentosDisparoServicoEnum.AO_ABRIR_ARTEFATO);
		return momentosDisparo;
	}

	@Override
	public String getNomeExibicao() {
		return "Persistência JDBC " + getVersao();
	}

	@Override
	protected Collection<IParametro> efetuaAcao() throws Exception {
		return efetuaOperacao();
	}

}
