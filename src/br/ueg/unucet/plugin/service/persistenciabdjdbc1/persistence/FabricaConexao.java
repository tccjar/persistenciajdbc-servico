package br.ueg.unucet.plugin.service.persistenciabdjdbc1.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class FabricaConexao {

	private static Connection conexao = null;
	private static String host;
    private static String database;
    private static String user;
    private static String password;
    private static String classeJDBC;

    public static void configureConexao(String host, String database,String user, String password,String classeJDBC)
    {
    	FabricaConexao.host = host;
    	FabricaConexao.database = database;
    	FabricaConexao.user = user;
    	FabricaConexao.password = password;
    	FabricaConexao.classeJDBC = classeJDBC;
    }
	public static Connection getConnection()
	{
		if (conexao == null)
		{
			try
			{
			        Class.forName(classeJDBC);
	                conexao = DriverManager.getConnection("jdbc:postgresql://" + host + "/" + database,user,password);
			}

			catch(SQLException e)
			{
				e.printStackTrace();
			}
			catch(ClassNotFoundException e)
			{
				e.printStackTrace();
			}
		}
		return conexao;
	}


}
 
