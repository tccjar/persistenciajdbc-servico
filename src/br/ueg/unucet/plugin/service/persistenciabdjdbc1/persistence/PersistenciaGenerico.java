package br.ueg.unucet.plugin.service.persistenciabdjdbc1.persistence;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import br.ueg.unucet.plugin.service.persistenciabdjdbc1.model.Entidade;
import br.ueg.unucet.plugin.service.persistenciabdjdbc1.model.EntidadePermanente;

/**
 * @author Tiago
 
 * @param <E>
 */
public abstract class PersistenciaGenerico<E extends Entidade>{

	private FabricaConexao fabricaConexao;
	protected Connection conexao;
	public CamposProcura camposProcura;
	private String limit = "";
	private CamposOrdem ordem = new CamposOrdem();
	private boolean inativos;

	public PersistenciaGenerico() {
		this.conexao = fabricaConexao.getConnection();
		this.camposProcura = new CamposProcura(getInstanciaEntidade().getClass());
	}

	public ArrayList<E> getLista() {
		String sql = "select * " + "from "
				+ (getInstanciaEntidade()).getNomeTabela() + " ";
		if (getInstanciaEntidade() instanceof EntidadePermanente) {
			sql += "where "
					+ ((EntidadePermanente) getInstanciaEntidade())
							.getCampoInativacao() + " = 1 ";
		}
		sql += " " + this.ordem.getSql() + " " + limit + " ";
		try {
			ArrayList<E> entidades = new ArrayList<E>();
			PreparedStatement stmt = this.conexao.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				E entidade = configuraCampos(rs);
				entidades.add(entidade);
			}
			rs.close();
			stmt.close();
			return entidades;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}

	}

	public ArrayList<E> procura() {
		String sql = "select * " + "from "
				+ (getInstanciaEntidade()).getNomeTabela() + " ";
		sql += "where ";
		if (getInstanciaEntidade() instanceof EntidadePermanente) {
			sql += ((EntidadePermanente) getInstanciaEntidade())
							.getCampoInativacao();
			if(inativos)
				sql += " = 0 ";
			else
				sql += " = 1 ";
			
			sql += " and "; 
		}
		sql += "(" + this.camposProcura.getSqlCampos() + ") ";
		sql += " " + this.ordem.getSql() + " " + limit + " ";
		try {
			ArrayList<E> entidades = new ArrayList<E>();
			PreparedStatement stmt = this.conexao.prepareStatement(sql);
			this.camposProcura.atribuirValores(stmt);
			ResultSet rs = executaQuery(stmt);
			while (rs.next()) {
				E novaEntidade = configuraCampos(rs);
				entidades.add(novaEntidade);
			}
			rs.close();
			stmt.close();
			return entidades;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
		
	}

	public E getById(Long id) {
		String sql = "select * " + "from "
				+ (getInstanciaEntidade()).getNomeTabela() + " ";
		sql += "where " + (getInstanciaEntidade()).getChavePrimaria() + " = ?";
		try {
			PreparedStatement stmt = this.conexao.prepareStatement(sql);
			stmt.setLong(1, id);
			ResultSet rs = executaQuery(stmt);
			E entidade = null;
			if (rs.next()) {
					entidade = configuraCampos(rs);
			}
			rs.close();
			stmt.close();
			return entidade;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}

	//para persistir � necess�rio alguns itens:
	//discernir entre persistencia e altera��o.
	//saber quais campos existem com os nomes na tabela
	//saber quais campos s�o �nicos
	//descobrir se j� n�o existe alguma entidade com campos �nicos na tabela(mandar pro controle)
	public final boolean persiste(Entidade entidade) {
		if (entidade.getCodigo() == null || entidade.getCodigo() == 0)
		{
			return insere(entidade);
		}
		else
		{
		  return altera(entidade);	
		}
		
	}

	/**
	 * @param entidade
	 * @return true se o item foi inserido
	 * 
	 */
	public boolean insere(Entidade entidade){
		String sql = "insert into "
				+ (getInstanciaEntidade()).getNomeTabela() + " ";
		CamposPersistencia campos = new CamposPersistencia(entidade);
		sql += campos.gerarSqlInsert();
		try
	    {
	      // prepared statement para inser��o
	      PreparedStatement stmt = conexao.prepareStatement(sql);
	      campos.configuraCamposInsert(stmt);
	      return executaQueryOperacao(stmt);
	      
	    }
	    catch (SQLException e) 
	    {
	      e.printStackTrace();
	      throw new RuntimeException();
	    }
		
	}
	
	public boolean altera(Entidade entidade){
		String sql = "update "
				+ (getInstanciaEntidade()).getNomeTabela() + " ";
		CamposPersistencia campos = new CamposPersistencia(entidade);
		sql += campos.gerarSqlUpdate();
		sql += " Where " + entidade.getChavePrimaria() + "=? ";
		try
	    {
	      // prepared statement para inser��o
	      PreparedStatement stmt = conexao.prepareStatement(sql);
	      campos.configuraCamposUpdate(stmt);
	      stmt.setLong(campos.getQuantidade() + 1, entidade.getCodigo());
	      return executaQueryOperacao(stmt);
	      
	    }
	    catch (SQLException e) 
	    {
	      e.printStackTrace();
	      throw new RuntimeException(); //Recebe o atributo do tipo Exception
	    }
	}
	
	public boolean remove(Entidade entidade) {
		if (entidade instanceof EntidadePermanente) {
			((EntidadePermanente) entidade).setStatus(0);
			persiste(entidade);
			return true;
		} else {
			String sql = "delete from "
					+ (getInstanciaEntidade()).getNomeTabela() + " ";
			sql += "where " + (getInstanciaEntidade()).getChavePrimaria()
					+ " = ?";
			try {
				// prepared statement para inser��o
				PreparedStatement stmt = conexao.prepareStatement(sql);
				// seta os valores
				stmt.setLong(1, entidade.getCodigo());
				stmt.execute();
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			    throw new RuntimeException(); //Recebe o atributo do tipo Exception
			}
		}
		return true;
	}

	protected abstract Entidade getInstanciaEntidade();

	protected abstract E configuraCampos(ResultSet rs) throws SQLException;
	
	public ResultSet executaQuery(PreparedStatement stmt) throws SQLException
	{
		System.out.println("Executando Query executada: " + stmt.toString());
		ResultSet rs = stmt.executeQuery();
		System.out.println("Query executada com sucesso para a entidade: " + getInstanciaEntidade().getClass().getCanonicalName());
		return rs;
		
	}
	
	public Boolean executaQueryOperacao(PreparedStatement stmt) throws SQLException
	{
		System.out.println("Executando Query executada: " + stmt.toString());
		stmt.execute();
		System.out.println("Query executada com sucesso para a entidade: " + getInstanciaEntidade().getClass().getCanonicalName());
		return true;
	}

	public void setLimit(Integer limit) {
		this.limit = "Limit " + limit;
	}

	@Deprecated
	public void addOrdem(Field f, String ascOuDesc) {
		this.ordem.add(f,ascOuDesc);
	}
	
	public void addOrdem(String nomeCampo, String ascOuDesc) {
		try {
		Field f;
		f = getInstanciaEntidade().getClass().getDeclaredField(nomeCampo);
		this.ordem.add(f,ascOuDesc);
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		}
	}
	
	public void limparLimiteOrdem()
	{
		this.limit = "";
		this.ordem = new CamposOrdem();
	}

	public void setExibirInativos(boolean b) {
		inativos = b;
	}
}
