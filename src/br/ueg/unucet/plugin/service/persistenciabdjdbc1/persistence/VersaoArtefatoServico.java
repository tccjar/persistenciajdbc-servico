package br.ueg.unucet.plugin.service.persistenciabdjdbc1.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;

import br.ueg.unucet.plugin.service.persistenciabdjdbc1.model.VersaoArtefato;

public class VersaoArtefatoServico extends PersistenciaGenerico<VersaoArtefato> {
	
	private VersaoArtefato entidade;
	
	@Override
	protected VersaoArtefato getInstanciaEntidade() {
		if (this.entidade == null)
			this.entidade = new VersaoArtefato();
		return this.entidade;
	}
	
	@Override
	protected VersaoArtefato configuraCampos(ResultSet rs)
			throws SQLException {
		this.entidade =  new VersaoArtefato();
		entidade.setCodigo(rs.getLong("codigo"));
		entidade.setArtefatoCodigo(rs.getLong("artefato_codigo"));
		entidade.setProjetoCodigo(rs.getLong("projeto_codigo"));
		entidade.setUltimaVersao(rs.getLong("ultima_versao"));
		entidade.setUltimaRevisao(rs.getLong("ultima_revisao"));
		entidade.setUsuarioPreenchedorCodigo(rs.getLong("usuario_preenchedor_codigo"));
				
		return entidade;
	}
}
