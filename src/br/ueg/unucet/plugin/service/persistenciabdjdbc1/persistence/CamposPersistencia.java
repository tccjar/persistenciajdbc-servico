package br.ueg.unucet.plugin.service.persistenciabdjdbc1.persistence;

import java.lang.reflect.Field;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import br.ueg.unucet.plugin.service.persistenciabdjdbc1.model.Entidade;
import br.ueg.unucet.plugin.service.persistenciabdjdbc1.util.annotations.LeitorAnnotacoes;
import br.ueg.unucet.plugin.service.persistenciabdjdbc1.util.annotations.Reflexao;

public class CamposPersistencia {
	
	private Integer quantidade = 0;
	private Entidade entidade;
	private ArrayList<Object> valores = new ArrayList<Object>();
	
	public CamposPersistencia(Entidade e)
	{
	  this.entidade = e;
	}
	
	public Integer getQuantidade()
	{
		return quantidade;
	}
	
	public String gerarSqlInsert()
	{
		String sql;
		sql = " (";
		Class<?> classe = entidade.getClass();
		for(Field f: Reflexao.getAllFields(classe))
		{
		  String nomeColuna = LeitorAnnotacoes.getNomeColuna(f);
		  if (nomeColuna != null && !LeitorAnnotacoes.isCampoAutoIncremento(f))
		  {
			  Object valor = Reflexao.InvokeGetMethod(entidade, f);
			  if (valor != null)
			  {
				sql += nomeColuna + ',';
				quantidade++;
			  }
		  }
		  
		}
		sql = sql.substring(0, sql.length() - 1);
		sql += " ) VALUES (";
		for(Integer i=0;i<quantidade;i++)
		{
			sql += "?,";
		}
		sql = sql.substring(0, sql.length() - 1);
		sql += ") ";
		return sql;
	}
	
	public String gerarSqlUpdate() {
		String sql;
		valores.clear();
		sql = " set ";
			Class<?> classe = entidade.getClass();
			for (Field f : Reflexao.getAllFields(classe)) {
				String nomeColuna = LeitorAnnotacoes.getNomeColuna(f);
				if (nomeColuna != null
						&& !LeitorAnnotacoes.isCampoAutoIncremento(f) ) {
					Object valor = Reflexao.InvokeGetMethod(entidade, f);
					if (valor != null)
					{	
						sql += nomeColuna + "=? ,";
						quantidade++;
						valores.add(valor);
					}
					//sql += nomeColuna + ',';
				}
				
			}
			sql = sql.substring(0, sql.length() - 1);
			sql += " ";
		return sql;
	}
	
	public void configuraCamposInsert(PreparedStatement stmt)
			throws SQLException {
		Integer iterador = 1;
		Class<?> classe = entidade.getClass();
		for (Field f : Reflexao.getAllFields(classe)) {
			String nomeColuna = LeitorAnnotacoes.getNomeColuna(f);
			if (nomeColuna != null
					&& !LeitorAnnotacoes.isCampoAutoIncremento(f)) {
				Object valor = Reflexao.InvokeGetMethod(entidade, f);
				    if (valor != null)
				    {
				    	Reflexao.InvokeStmtMethod(f, valor, stmt, iterador);
				    	iterador++;
				    }
			}

		}
	}
	

	public void configuraCamposUpdate(PreparedStatement stmt)
			throws SQLException {
		Integer iterador = 1;
		Class<?> classe = entidade.getClass();
		for (Field f : Reflexao.getAllFields(classe)) {
			String nomeColuna = LeitorAnnotacoes.getNomeColuna(f);
			if (nomeColuna != null
					&& !LeitorAnnotacoes.isCampoAutoIncremento(f)) {
				Object valor = Reflexao.InvokeGetMethod(entidade, f);
				if (valor != null) {

					Reflexao.InvokeStmtMethod(f, valor, stmt, iterador);
					iterador++;
				}
			}

		}
	}
	
	
	public void reinicilizar()
	{
		quantidade = 0;
		valores.clear();
	}
	
}