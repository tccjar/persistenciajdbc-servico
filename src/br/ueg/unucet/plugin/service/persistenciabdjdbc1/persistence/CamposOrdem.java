package br.ueg.unucet.plugin.service.persistenciabdjdbc1.persistence;

import java.lang.reflect.Field;
import java.util.ArrayList;

import br.ueg.unucet.plugin.service.persistenciabdjdbc1.util.annotations.LeitorAnnotacoes;

public class CamposOrdem {

	ArrayList<Field> campos = new ArrayList<Field>();
	ArrayList<String> ordem = new ArrayList<String>();
	
	public String getSql()
	{
		String retorno = "";
		for(int i=0;i< campos.size(); i++)
		{
			Field f = campos.get(i);
			String nomeColuna = LeitorAnnotacoes.getNomeColuna(f);
			retorno += nomeColuna + " " + ordem.get(i) + ", "; 
		}
		if (campos.size() > 0)
		{	
			retorno = "order by " + retorno;
			retorno = retorno.substring(0, retorno.length() - 2);
		}
		return retorno;
	}
	
	public void add(Field f, String ascOuDesc)
	{
		this.campos.add(f);
		this.ordem.add(ascOuDesc);
	}
}
