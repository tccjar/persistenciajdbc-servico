package br.ueg.unucet.plugin.service.persistenciabdjdbc1.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;

import br.ueg.unucet.plugin.service.persistenciabdjdbc1.model.Artefato;
import br.ueg.unucet.plugin.service.persistenciabdjdbc1.model.Entidade;

public class ArtefatoServico extends PersistenciaGenerico<Artefato> {

	private Artefato entidade;
	
	@Override
	protected Entidade getInstanciaEntidade() {
		if (this.entidade == null)
			this.entidade = new Artefato();
		return this.entidade;
	}

	@Override
	protected Artefato configuraCampos(ResultSet rs)
			throws SQLException {
		Artefato entidade = new Artefato();

		entidade.setCodigo(rs.getLong("codigo"));
		entidade.setProjetoCodigo(rs.getLong("projeto_codigo"));
		entidade.setArtefatoCodigo(rs.getLong("artefato_codigo"));
		entidade.setVersao(rs.getLong("versao"));
		entidade.setRevisao(rs.getLong("revisao"));
		entidade.setListaMembros(rs.getBytes("lista_membro"));
		entidade.setListaValoresMembros(rs.getBytes("valores_membro"));
		return entidade;
	}

}
