package br.ueg.unucet.plugin.service.persistenciabdjdbc1.persistence;

import java.lang.reflect.Field;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import br.ueg.unucet.plugin.service.persistenciabdjdbc1.util.annotations.LeitorAnnotacoes;



public class CamposProcura {
  
	private ArrayList<Field> campos ;
	private ArrayList<String> valores;
	private int posicaoCampo = 0;
	private int posicaoValor = 0;
	private String condicional = "OR";
	private Class<?> classeEntidade;
	
	public CamposProcura(Class<?> classe)
	{
		this.classeEntidade = classe;
		campos = new ArrayList<Field>();
		valores = new ArrayList<String>();
	}
	
	@Deprecated
	public void add(Field campo, String valor)
	{
		campos.add(campo);
		valores.add("%" + valor + "%");
	}
	
	public void add(String nomeCampo, String valor) {
		Field campo;
		try {
			campo = classeEntidade.getDeclaredField(nomeCampo);
			campos.add(campo);
			valores.add("%" + valor + "%");
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		}
	}
	
	public void add(String nomeCampo, String valor, Boolean exatamenteIgual) {
		Field campo;
		try {
			campo = classeEntidade.getDeclaredField(nomeCampo);
			campos.add(campo);
			String retorno = "";
			if (!exatamenteIgual) {
				retorno += "%";
			}
			retorno += valor;
			if (!exatamenteIgual) {
				retorno += "%";
			}
			valores.add(valor);
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		}
	}
	
	@Deprecated
	public void add(Field campo, String valor, Boolean exatamenteIgual)
	{
		campos.add(campo);
		String retorno = "";
		if(!exatamenteIgual)
		{
			retorno+= "%";
		}
		retorno += valor;
		if(!exatamenteIgual)
		{
			retorno+= "%";
		}
		valores.add(valor);
	}
	
	public void limpar()
	{
		campos.clear();
	}
	
	//Pega apenas um dos campos configurados
	private Field getCampo()
	{
	  if (campos.size() >= posicaoCampo + 1)
	  {
	      Field campo = campos.get(posicaoCampo);
		  posicaoCampo++;
		  return campo;
	    
	  }
	  else
	  {	  
		  posicaoCampo = 0;
	  }
	  return null;    
	}
	
	private String getValor()
	{
	  if (valores.size() >= posicaoValor + 1)
	  {
		  String valor = valores.get(posicaoValor);
		  posicaoValor++;
		  return valor;
	  }
	  else
	  {	  
		  posicaoValor = 0;
	  }
	  return null;
	}
	
	//Gera uma sql com dos campos
	//exemplo: (campo1 like '%?%) or (campo2 like '%?%') or (campo3 like like '%?%')
	//OBS: Fun��o Retorna iterador para o in�cio
	public String getSqlCampos()
	{
	  String sql = "";
	  Field campo;
	  posicaoCampo = 0;
	  campo = getCampo();
	  while ( campo != null )
	  {
		  //sql += " (" + campo.getName() + " like CONCACT('%' as text, ? , '%' as text) ) ";
		  sql += " ( CAST(" + LeitorAnnotacoes.getNomeColuna(campo) + " as text)" + " like ? ) ";
		  if ((campos.size() >= posicaoCampo + 1) ) //se n�o tiver chegado ao fim da lista
			  sql += this.condicional + " ";
		  campo = getCampo();
	  }
	  campos.clear();
	  return sql;
	}
	
	public void atribuirValores(PreparedStatement stmt ) throws SQLException
	{
		  String valor;
		  posicaoValor = 0;
		  valor = getValor();
		  while ( valor != null )
		  {
			  stmt.setString(posicaoValor, valor);
			  valor = getValor();
		  } 
		  valores.clear();
	}
	
	public void setCondicional(String orOuAnd)
	{
		this.condicional = orOuAnd;
	}

	public ArrayList<Field> getCampos() {
		return campos;
	}
}
